local POWERMANAGEMENT = {};

local component = require("component")
local computer = require("computer")
local sides = require("sides")
--local rs = component.redstone
local bot = require("robot")
local gen = component.generator
local fuelSlot = 0

function POWERMANAGEMENT:getEnergyPercentRemaining()  
  return ( computer.energy() / computer.maxEnergy() ) * 100
end

function POWERMANAGEMENT:chargeAtStation()
  local i = 0
  local originalColor = bot.getLightColor()
  local timesRemained = 0
  local currentCharge = self.getEnergyPercentRemaining()
  local chargeSuccess = true
  --enable charging
  --[[
  rs.setOutput(sides.top, 15)
  rs.setOutput(sides.bottom, 15)

  for i = 1, 4, 1 do
    if bot.detect() == true then 
      rs.setOutput(sides.front, 15) 
      break
    else
      bot.turnRight()
    end
  end
  --]]

  bot.setLightColor(0x00FF00)
  while self.getEnergyPercentRemaining() < 98 do
    os.sleep(1)
    
    if self.getEnergyPercentRemaining() <= currentCharge then
      timesRemained = timesRemained + 1
    else
      timesRemained = 0
      currentCharge = self.getEnergyPercentRemaining()
    end
    if timesRemained > 4 then
      chargeSuccess = false
      break
    end
  end
  bot.setLightColor(originalColor)
  --for i = 0, 5, 1 do rs.setOutput(i, 0) end

  return chargeSuccess
end

function POWERMANAGEMENT:setFuelSlot(slot)
  self.fuelSlot = slot
end

function POWERMANAGEMENT:monitorBattery()
  local pow = self.getEnergyPercentRemaining()
  if pow < 15 and gen.count() == 0 then
    if bot.count(self.fuelSlot) > 0 then
      bot.select(self.fuelSlot)
      gen.insert(1)
    end
  end
end

return POWERMANAGEMENT;