local task = {
  ["waypoints"] = {
    {
      ["coord"] = {272,45,309},
      ["location"] = "Machine Room - Sag Mill",
      ["action"] = "DUMP_INVENTORY"
    },
    {
      ["coord"] = {266,45,309}
    },
    {
      ["coord"] = {266,63,339}
    },
    {
      ["coord"] = {253,69,339}
    },
    {
      ["coord"] = {253,70,329}
    },
    {
      ["coord"] = {242,69,329}
    },
    {
      ["coord"] = {242,69,325},
      ["location"] = "Robot Charging Station",
      ["action"] = "OPTIONAL_CHARGE",
      ["endpoint"] = true
    },
    {
      ["coord"] = {242,69,323}
    },
    {
      ["coord"] = {225,66,323}
    },
    {
      ["coord"] = {225,65,302}
    },
    {
      ["coord"] = {183,65,298},
      ["location"] = "Iron Excavator",
      ["action"] = "FILL_INVENTORY"
    }
  },
  ["mission"] = "CARGO_HAUL",
  ["navigation_strategy"] = "PING_PONG"
};
return task;