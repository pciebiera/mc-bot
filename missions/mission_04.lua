local task = {
  ["waypoints"] = {
    {
      ["coord"] = {242,69,325},
      ["location"] = "Robot Charging Station",
      ["action"] = "OPTIONAL_CHARGE"
    },
    {
      ["coord"] = {242,69,324}
    },
    {
      ["coord"] = {208,66,325},
      ["location"] = "Orchard - Pomegranate Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {200,66,325},
      ["location"] = "Orchard - Pear Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {192,66,325},
      ["location"] = "Orchard - Plum Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {184,66,325},
      ["location"] = "Orchard - Chestnut Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {176,66,325},
      ["location"] = "Orchard - Apricot Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {168,66,325},
      ["location"] = "Orchard - Persimmon Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {160,66,325},
      ["location"] = "Orchard - Fig Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {152,66,325},
      ["location"] = "Orchard - Date Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {144,66,325},
      ["location"] = "Orchard - Walnut Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {148,66,333},
      ["location"] = "Orchard - Avocado Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {134,69,334},
      ["location"] = "Orchard - Peppercorn Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {125,69,334},
      ["location"] = "Orchard - Peppercorn Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {117,69,334},
      ["location"] = "Orchard - Walnut Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {116,69,334},
    },
    {
      ["coord"] = {109,69,325},
      ["location"] = "Orchard - Maple Tree",
      ["action"] = "HARVEST_TRUNK"
    },
    {
      ["coord"] = {117,69,325},
      ["location"] = "Orchard - Nutmeg Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {125,69,325},
      ["location"] = "Orchard - Nutmeg Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {133,69,324},
      ["location"] = "Orchard - Cinnamon Tree",
      ["action"] = "HARVEST_TRUNK"
    },
    {
      ["coord"] = {134,69,313},
      ["location"] = "Orchard - Cinnamon Tree",
      ["action"] = "HARVEST_TRUNK"
    },
    {
      ["coord"] = {144,66,315},
      ["location"] = "Orchard - Coconut Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {152,66,315},
      ["location"] = "Orchard - Pistachio Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {160,66,315},
      ["location"] = "Orchard - Apple Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {168,66,315},
      ["location"] = "Orchard - Pecan Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {176,66,315},
      ["location"] = "Orchard - Papaya Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {184,66,315},
      ["location"] = "Orchard - Cherry Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {192,66,315},
      ["location"] = "Orchard - Gooseberry Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {200,66,315},
      ["location"] = "Orchard - Cashew Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {208,66,315},
      ["location"] = "Orchard - Almond Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {208,66,309},
      ["location"] = "Orchard - Orange Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {200,66,309},
      ["location"] = "Orchard - Lime Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {192,66,309},
      ["location"] = "Orchard - Grapefruit Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {184,66,309},
      ["location"] = "Orchard - Dragonfruit Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {176,66,309},
      ["location"] = "Orchard - Starfruit Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {168,66,309},
      ["location"] = "Orchard - Lemon Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {160,66,309},
      ["location"] = "Orchard - Mango Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {152,66,309},
      ["location"] = "Orchard - Peach Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {144,66,309},
      ["location"] = "Orchard - Banana Tree",
      ["action"] = "HARVEST_FRUIT"
    },
    {
      ["coord"] = {224,66,309}
    },
    {
      ["coord"] = {224,66,323}
    },
    {
      ["coord"] = {251,69,323}
    },
    {
      ["coord"] = {251,69,328}
    },
    {
      ["coord"] = {256,71,328},
      ["location"] = "Kitchen - Fridge 5",
      ["action"] = "STORE_LIST",
      ["managedinventory"] = require("inventory.fridge05")
    },
    {
      ["coord"] = {242,69,328}
    },
    {
      ["coord"] = {242,69,325},
      ["location"] = "Robot Charging Station",
      ["action"] = "OPTIONAL_CHARGE",
      ["endpoint"] = true
    }
  },
  ["mission"] = "MANAGE_INVENTORY",
  ["navigation_strategy"] = "ONE_CYCLE"
};
return task;