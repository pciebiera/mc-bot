local task = {
  ["waypoints"] = {
    {
      ["coord"] = {242,69,325},
      ["location"] = "Robot Charging Station",
      ["action"] = "OPTIONAL_CHARGE"
    },
    {
      ["coord"] = {242,69,327}
    },
    {
      ["coord"] = {255,71,324},
      ["location"] = "Kitchen - Fridge 1",
      ["action"] = "BUILD_LIST",
      ["managedinventory"] = require("inventory.fridge01")
    },
    {
      ["coord"] = {256,71,324},
      ["location"] = "Kitchen - Fridge 2",
      ["action"] = "BUILD_LIST",
      ["managedinventory"] = require("inventory.fridge02")
    },
    {
      ["coord"] = {257,71,324},
      ["location"] = "Kitchen - Fridge 3",
      ["action"] = "BUILD_LIST",
      ["managedinventory"] = require("inventory.fridge03")
    },
    {
      ["coord"] = {253,70,324}
    },
    {
      ["coord"] = {253,69,331}
    },
    {
      ["coord"] = {262,72,331}
    },
    {
      ["coord"] = {262,70,316}
    },
    {
      ["coord"] = {279,73,316}
    },
    {
      ["coord"] = {279,73,321},
      ["location"] = "Top of Back Stairs"
    },
    {
      ["coord"] = {298,64,321},
      ["location"] = "Bottom of Back Stairs"
    },
    {
      ["coord"] = {298,64,327}
    },
    {
      ["coord"] = {305,64,334}
    },
    {
      ["coord"] = {326,66,334}
    },
    {
      ["coord"] = {326,66,326},
      ["location"] = "Animal Processing - Storage 1",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {326,66,325},
      ["location"] = "Animal Processing - Storage 2",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {326,66,324},
      ["location"] = "Animal Processing - Storage 3",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {326,66,321},
      ["location"] = "Animal Processing - XP Storage",
      ["action"] = "TAKE_FLUID"
    },
    {
      ["coord"] = {326,66,311}
    },
    {
      ["coord"] = {298,63,311}
    },
    {
      ["coord"] = {298,64,321},
      ["location"] = "Bottom of Back Stairs"
    },
    {
      ["coord"] = {279,73,321},
      ["location"] = "Top of Back Stairs"
    },
    {
      ["coord"] = {279,73,316}
    },
    {
      ["coord"] = {262,70,316}
    },
    {
      ["coord"] = {262,72,331}
    },
    {
      ["coord"] = {253,69,331}
    },
    {
      ["coord"] = {253,70,324}
    },
    {
      ["coord"] = {255,71,324},
      ["location"] = "Kitchen - Fridge 1",
      ["action"] = "STORE_LIST",
      ["managedinventory"] = require("inventory.fridge01")
    },
    {
      ["coord"] = {256,71,324},
      ["location"] = "Kitchen - Fridge 2",
      ["action"] = "STORE_LIST",
      ["managedinventory"] = require("inventory.fridge02")
    },
    {
      ["coord"] = {257,71,324},
      ["location"] = "Kitchen - Fridge 3",
      ["action"] = "STORE_LIST",
      ["managedinventory"] = require("inventory.fridge03")
    },
    {
      ["location"] = "Store XP",
      ["coord"] = {248,69,324}
    },
    {
      ["coord"] = {248,69,323}
    },
    {
      ["coord"] = {242,69,323}
    },
    {
      ["coord"] = {242,69,325},
      ["location"] = "Robot Charging Station",
      ["action"] = "OPTIONAL_CHARGE",
      ["endpoint"] = true
    }
  },
  ["mission"] = "MANAGE_INVENTORY",
  ["navigation_strategy"] = "ONE_CYCLE"
};
return task;