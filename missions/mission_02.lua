local task = {
  ["waypoints"] = {
    {
      ["coord"] = {242,69,325},
      ["location"] = "Robot Charging Station",
      ["action"] = "OPTIONAL_CHARGE"
    },
    {
      ["coord"] = {242,69,328}
    },
    {
      ["coord"] = {257,71,328},
      ["location"] = "Kitchen - Fridge 4",
      ["action"] = "BUILD_LIST",
      ["managedinventory"] = require("inventory.fridge04")
    },
    {
      ["coord"] = {256,71,324},
      ["location"] = "Kitchen - Fridge 2",
      ["action"] = "BUILD_LIST",
      ["managedinventory"] = require("inventory.fridge02")
    },
    {
      ["coord"] = {252,69,324}
    },
    {
      ["coord"] = {252,70,349},
      ["location"] = "Garden Cloche 1",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,351},
      ["location"] = "Garden Cloche 2",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,353},
      ["location"] = "Garden Cloche 3",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,355},
      ["location"] = "Garden Cloche 4",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,357},
      ["location"] = "Garden Cloche 5",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,359},
      ["location"] = "Garden Cloche 6",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,361},
      ["location"] = "Garden Cloche 7",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,363},
      ["location"] = "Garden Cloche 8",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,365},
      ["location"] = "Garden Cloche 9",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,367},
      ["location"] = "Garden Cloche 10",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,69,368}
    },
    {
      ["coord"] = {256,69,368}
    },
    {
      ["coord"] = {256,70,367},
      ["location"] = "Garden Cloche 11",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,365},
      ["location"] = "Garden Cloche 12",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,363},
      ["location"] = "Garden Cloche 13",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,361},
      ["location"] = "Garden Cloche 14",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,359},
      ["location"] = "Garden Cloche 15",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,357},
      ["location"] = "Garden Cloche 16",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,355},
      ["location"] = "Garden Cloche 17",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,353},
      ["location"] = "Garden Cloche 18",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,351},
      ["location"] = "Garden Cloche 19",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,349},
      ["location"] = "Garden Cloche 20",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,69,348}
    },
    {
      ["coord"] = {252,69,348}
    },
    {
      ["coord"] = {252,70,349},
      ["location"] = "Garden Cloche 1",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche01")
    },
    {
      ["coord"] = {252,70,351},
      ["location"] = "Garden Cloche 2",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche02")
    },
    {
      ["coord"] = {252,70,353},
      ["location"] = "Garden Cloche 3",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche03")
    },
    {
      ["coord"] = {252,70,355},
      ["location"] = "Garden Cloche 4",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche04")
    },
    {
      ["coord"] = {252,70,357},
      ["location"] = "Garden Cloche 5",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche05")
    },
    {
      ["coord"] = {252,70,359},
      ["location"] = "Garden Cloche 6",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche06")
    },
    {
      ["coord"] = {252,70,361},
      ["location"] = "Garden Cloche 7",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche07")
    },
    {
      ["coord"] = {252,70,363},
      ["location"] = "Garden Cloche 8",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche08")
    },
    {
      ["coord"] = {252,70,365},
      ["location"] = "Garden Cloche 9",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche09")
    },
    {
      ["coord"] = {252,70,367},
      ["location"] = "Garden Cloche 10",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche10")
    },
    {
      ["coord"] = {252,69,368}
    },
    {
      ["coord"] = {256,69,368}
    },
    {
      ["coord"] = {256,70,367},
      ["location"] = "Garden Cloche 11",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche11")
    },
    {
      ["coord"] = {256,70,365},
      ["location"] = "Garden Cloche 12",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche12")
    },
    {
      ["coord"] = {256,70,363},
      ["location"] = "Garden Cloche 13",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche13")
    },
    {
      ["coord"] = {256,70,361},
      ["location"] = "Garden Cloche 14",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche14")
    },
    {
      ["coord"] = {256,70,359},
      ["location"] = "Garden Cloche 15",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche15")
    },
    {
      ["coord"] = {256,70,357},
      ["location"] = "Garden Cloche 16",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche16")
    },
    {
      ["coord"] = {256,70,355},
      ["location"] = "Garden Cloche 17",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche17")
    },
    {
      ["coord"] = {256,70,353},
      ["location"] = "Garden Cloche 18",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche18")
    },
    {
      ["coord"] = {256,70,351},
      ["location"] = "Garden Cloche 19",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche19")
    },
    {
      ["coord"] = {256,70,349},
      ["location"] = "Garden Cloche 20",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = require("inventory.cloche20")
    },
    {
      ["coord"] = {253,69,339}
    },
    {
      ["coord"] = {253,70,328}
    },
    { 
      ["coord"] = {257,71,328},
      ["location"] = "Kitchen - Fridge 4",
      ["action"] = "STORE_LIST",
      ["managedinventory"] = require("inventory.fridge04")
    },
    {
      ["coord"] = {256,71,324},
      ["location"] = "Kitchen - Fridge 2",
      ["action"] = "STORE_LIST",
      ["managedinventory"] = require("inventory.fridge02")
    },
    {
      ["coord"] = {248,69,326},
      ["location"] = "Trashcan",
      ["action"] = "DISPOSE_CULL"
    },
    {
      ["coord"] = {248,69,327}
    },
    {
      ["coord"] = {242,69,327}
    },
    {
      ["coord"] = {242,69,325},
      ["location"] = "Robot Charging Station",
      ["action"] = "OPTIONAL_CHARGE",
      ["endpoint"] = true
    }
  },
  ["mission"] = "MANAGE_INVENTORY",
  ["navigation_strategy"] = "ONE_CYCLE"
};
return task;