local MI=
{
  ["name"] = "Kitchen - Fridge 2",
  ["data"] = {
    { ["slot"] = 1,  ["item"] = "harvestcraft:garlicitem",        ["target"] = 64 },
    { ["slot"] = 2,  ["item"] = "harvestcraft:garlicitem",        ["target"] = 64 },
    { ["slot"] = 3,  ["item"] = "harvestcraft:garlicitem",        ["target"] = 64 },
    { ["slot"] = 4,  ["item"] = "harvestcraft:beanitem",          ["target"] = 64 },
    { ["slot"] = 5,  ["item"] = "harvestcraft:beanitem",          ["target"] = 64 },
    { ["slot"] = 6,  ["item"] = "harvestcraft:beanitem",          ["target"] = 64 },
    { ["slot"] = 7,  ["item"] = "harvestcraft:avocadoitem",       ["target"] = 64 },
    { ["slot"] = 8,  ["item"] = "harvestcraft:avocadoitem",       ["target"] = 64 },
    { ["slot"] = 9,  ["item"] = "harvestcraft:avocadoitem",       ["target"] = 64 },
    { ["slot"] = 10, ["item"] = "harvestcraft:sweetpotatoitem",   ["target"] = 64 },
    { ["slot"] = 11, ["item"] = "harvestcraft:sweetpotatoitem",   ["target"] = 64 },
    { ["slot"] = 12, ["item"] = "harvestcraft:sweetpotatoitem",   ["target"] = 64 },
    { ["slot"] = 13, ["item"] = "harvestcraft:spinachitem",       ["target"] = 64 },
    { ["slot"] = 14, ["item"] = "harvestcraft:spinachitem",       ["target"] = 64 },
    { ["slot"] = 15, ["item"] = "harvestcraft:spinachitem",       ["target"] = 64 },
    { ["slot"] = 16, ["item"] = "harvestcraft:gigapickleitem",    ["target"] = 64 },
    { ["slot"] = 17, ["item"] = "harvestcraft:gigapickleitem",    ["target"] = 64 },
    { ["slot"] = 18, ["item"] = "harvestcraft:gigapickleitem",    ["target"] = 64 },
    { ["slot"] = 19, ["item"] = "harvestcraft:riceitem",          ["target"] = 64 },
    { ["slot"] = 20, ["item"] = "harvestcraft:riceitem",          ["target"] = 64 },
    { ["slot"] = 21, ["item"] = "harvestcraft:riceitem",          ["target"] = 64 },
    { ["slot"] = 22, ["item"] = "harvestcraft:celeryitem",        ["target"] = 64 },
    { ["slot"] = 23, ["item"] = "harvestcraft:celeryitem",        ["target"] = 64 },
    { ["slot"] = 24, ["item"] = "harvestcraft:celeryitem",        ["target"] = 64 },
    { ["slot"] = 25, ["item"] = "minecraft:carrot",               ["target"] = 64 },
    { ["slot"] = 26, ["item"] = "minecraft:carrot",               ["target"] = 64 },
    { ["slot"] = 27, ["item"] = "minecraft:carrot",               ["target"] = 64 },
    { ["slot"] = 28, ["item"] = "harvestcraft:zucchiniitem",      ["target"] = 64 },
    { ["slot"] = 29, ["item"] = "harvestcraft:zucchiniitem",      ["target"] = 64 },
    { ["slot"] = 30, ["item"] = "harvestcraft:zucchiniitem",      ["target"] = 64 },
    { ["slot"] = 31, ["item"] = "harvestcraft:whitemushroomitem", ["target"] = 64 },
    { ["slot"] = 32, ["item"] = "harvestcraft:whitemushroomitem", ["target"] = 64 },
    { ["slot"] = 33, ["item"] = "harvestcraft:whitemushroomitem", ["target"] = 64 },
    { ["slot"] = 34, ["item"] = "minecraft:beetroot",             ["target"] = 64 },
    { ["slot"] = 35, ["item"] = "minecraft:beetroot",             ["target"] = 64 },
    { ["slot"] = 36, ["item"] = "minecraft:beetroot",             ["target"] = 64 },
    { ["slot"] = 37, ["item"] = "harvestcraft:chilipepperitem",   ["target"] = 64 },
    { ["slot"] = 38, ["item"] = "harvestcraft:chilipepperitem",   ["target"] = 64 },
    { ["slot"] = 39, ["item"] = "harvestcraft:chilipepperitem",   ["target"] = 64 },
    { ["slot"] = 40, ["item"] = "harvestcraft:soybeanitem",       ["target"] = 64 },
    { ["slot"] = 41, ["item"] = "harvestcraft:soybeanitem",       ["target"] = 64 },
    { ["slot"] = 42, ["item"] = "harvestcraft:soybeanitem",       ["target"] = 64 },
    { ["slot"] = 43, ["item"] = "harvestcraft:cucumberitem",      ["target"] = 64 },
    { ["slot"] = 44, ["item"] = "harvestcraft:cucumberitem",      ["target"] = 64 },
    { ["slot"] = 45, ["item"] = "harvestcraft:cucumberitem",      ["target"] = 64 },
    { ["slot"] = 46, ["item"] = "harvestcraft:broccoliitem",      ["target"] = 64 },
    { ["slot"] = 47, ["item"] = "harvestcraft:broccoliitem",      ["target"] = 64 },
    { ["slot"] = 48, ["item"] = "harvestcraft:broccoliitem",      ["target"] = 64 },
    { ["slot"] = 49, ["item"] = "minecraft:potato",               ["target"] = 64 },
    { ["slot"] = 50, ["item"] = "minecraft:potato",               ["target"] = 64 },
    { ["slot"] = 51, ["item"] = "minecraft:potato",               ["target"] = 64 },
    { ["slot"] = 52, ["item"] = "harvestcraft:asparagusitem",     ["target"] = 64 },
    { ["slot"] = 53, ["item"] = "harvestcraft:asparagusitem",     ["target"] = 64 },
    { ["slot"] = 54, ["item"] = "harvestcraft:asparagusitem",     ["target"] = 64 }
  }
};
return MI;