local AG={};

local comp = require("component")
local bot = require("robot")
local sides = require("sides")


function AG:pickFruitCheckAbove()
  local b, r = bot.detectUp()
  if b == true then
    if r == "passable" then
      bot.useUp(sides.top)
      return true
    elseif r == "solid" then
      return true
    end
  end
  return false
end

function AG:pickFruit()
  local foundTop = false
  while foundTop == false do
    bot.turnRight()
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    bot.turnLeft()
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    bot.turnLeft()
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    bot.turnLeft()
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    bot.turnLeft()
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    bot.turnLeft()
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    bot.turnLeft()
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    bot.turnLeft()
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    bot.turnLeft()
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    while bot.forward() == false do end
    if self:pickFruitCheckAbove() == true then foundTop = true end
    bot.turnLeft()
    while bot.forward() == false do end
    if foundTop ~= true then bot.up() end
  end
    while bot.detectDown() ~= true do bot.down() end
end

function AG:harvestBark()
  while bot.detectUp() == false do 
    bot.use(sides.front)
    bot.up()
  end
  bot.use(sides.front)
  while bot.detectDown == false do bot.down() end
end
return AG;