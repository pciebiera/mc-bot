local template = {
  ["managedinventory"] = {
    { ["slot"] = 1,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 },
    { ["slot"] = 2,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 },
    { ["slot"] = 3,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 },
    { ["slot"] = 4,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 },
    { ["slot"] = 5,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 },
    { ["slot"] = 6,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 },
    { ["slot"] = 37, ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 },
    { ["slot"] = 38, ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 },
    { ["slot"] = 39, ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 },
    { ["slot"] = 46, ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 },
    { ["slot"] = 47, ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 },
    { ["slot"] = 48, ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 },
  },
  ["managedinventory"] = {
    { ["slot"] = 1,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
    { ["slot"] = 2,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
    { ["slot"] = 3,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
    { ["slot"] = 4,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
    { ["slot"] = 10, ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
    { ["slot"] = 11, ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
    { ["slot"] = 12, ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
    { ["slot"] = 13, ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
    { ["slot"] = 19, ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
    { ["slot"] = 20, ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
    { ["slot"] = 21, ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
    { ["slot"] = 22, ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
    { ["slot"] = 28, ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
    { ["slot"] = 29, ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
    { ["slot"] = 30, ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
    { ["slot"] = 31, ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
    { ["slot"] = 37, ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
    { ["slot"] = 38, ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
    { ["slot"] = 39, ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
    { ["slot"] = 40, ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
    { ["slot"] = 46, ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 },
    { ["slot"] = 47, ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 },
    { ["slot"] = 48, ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 },
    { ["slot"] = 49, ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 }
  }
    
};
return template;