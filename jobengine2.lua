job = require("missions.mission_04")
working = true
comp = require("component")
nav = require("navigation")
bot = require("robot")
sides = require("sides")
pc = require("computer")
pwr = require("powermanagement")
inv = require("inventorymanagement")
invcnt = comp.inventory_controller
agri = require("agriculture")

bot.setLightColor(0xff0000)

currentWaypointIndex = 0
waypointOrder = 1
inventorySlots = {}
foundStart = false
local fuelSlot = 0

shoppingList = {}



cx,cy,cz = nav.getPosition()

if job.navigation_strategy == "PING_PONG" then
  for i = 1, #job.waypoints, 1 do
    if job.waypoints[i].coord[1] == cx and job.waypoints[i].coord[2] == cy and job.waypoints[i].coord[3] == cz then
      foundStart = true
      currentWaypointIndex = i
      print("Found waypoint start...")
      break
    end
  end
elseif job.navigation_strategy == "ONE_CYCLE" then
  if job.waypoints[1].coord[1] == cx and job.waypoints[1].coord[2] == cy and job.waypoints[1].coord[3] == cz then
    foundStart = true
    currentWaypointIndex = 1
  end
  --foundStart = true
end

inventorySize = bot.inventorySize()
if inventorySize == nil or inventorySize == 0 then
  working = false
  print("Could not determine inventory size")
else
  print("Found inventory...")
  for i = 1, inventorySize, 1 do
    if bot.count(i) == 0 then
      table.insert(inventorySlots, i)
      print("Found usable inventory slot for job...")
    else
      local _info = invcnt.getStackInInternalSlot(i)
      if _info ~= nil then
        if _info.name == "minecraft:coal" or _info.name == "minecraft:coalblock" then
          if fuelSlot == 0 then 
            print("Found fuel at slot " .. i .."\n")
            fuelSlot = i
            pwr:setFuelSlot(fuelSlot)
          end 
        end
      end
    end
  end
  inv:setAvailableSlots(inventorySlots)
end

if #inventorySlots == 0 then
  working = false
  print("There are no free inventory slots available for this job")
end

if foundStart == false then
  working = false
  print("Could not locate starting point for job")
end

if job == nil then 
  working = false
  print("Could not get job")
end



function printList()
  for q = 1, #shoppingList, 1 do
    io.write("Item #" .. q .. " " .. shoppingList[q].item .. " Qty: " .. shoppingList[q].qty .. "\n")
  end
end


--currentWaypointIndex = 33
while working do
  local x = 0
  local y = 0
  local z = 0
  local i = 0
  local e = 0
  local t = 0
  local thisWaypoint = job.waypoints[currentWaypointIndex]
  
  if thisWaypoint.action == "OPTIONAL_CHARGE" then
    if pwr:getEnergyPercentRemaining() < 30 then pwr:chargeAtStation() end
  end
  if thisWaypoint.action == "FILL_INVENTORY" then inv:getAllItems() end
  if thisWaypoint.action == "DUMP_INVENTORY" then inv:putAllItems() end

  if thisWaypoint.action == "BUILD_LIST" then
    if #shoppingList == 0 then
      shoppingList = inv:getShoppingList(thisWaypoint.managedinventory.data)
    else
      local tList = inv:getShoppingList(thisWaypoint.managedinventory.data)
      if #tList > 0 then
        for i = 1, #tList, 1 do
          local tFound = false
          for e = 1, #shoppingList, 1 do
            if tList[i].item == shoppingList[e].item then
              shoppingList[e].qty = shoppingList[e].qty + tList[i].qty
              tFound = true
              break
            end
          end
          if tFound == false then
            table.insert(shoppingList, tList[i])
          end
          tFound = false
        end
      end
    end
  end

  if thisWaypoint.action == "SUPPLY_LIST" then
    local needToShop = false
    for i = 1, #shoppingList, 1 do
      if shoppingList[i].qty > 0 then
        needToShop = true
        break
      end
    end

    if needToShop == true then
      inv:goShopping(shoppingList)
    end
  end

  if thisWaypoint.action == "STORE_LIST" then
    inv:restockInventory(thisWaypoint.managedinventory.data)
  end

  if thisWaypoint.action == "PICK_CULL" then
    inv:pickItemForCulling(thisWaypoint.managedinventory.data)
  end

  if thisWaypoint.action == "DISPOSE_CULL" then
    inv:putAllItems()
  end

  if thisWaypoint.action == "HARVEST_FRUIT" or thisWaypoint.action == "HARVEST_TRUNK" then
    t = 0
    while t < 5 do
      i, e = bot.detect(sides.front)
      if i ~= true or e ~= "solid" then
        bot.turnRight()
      else
        if inv:selectNextOpenSlot() == false then break end
        if thisWaypoint.action == "HARVEST_FRUIT" then
          agri:pickFruit()
        elseif thisWaypoint.action == "HARVEST_TRUNK" then
          agri:harvestBark()
        end
        break
      end
      t = t + 1
    end
  end

  if currentWaypointIndex == #job.waypoints then
    if job.navigation_strategy == "ONE_CYCLE" then return 0 end
    waypointOrder = -1
  elseif currentWaypointIndex == 1 then
    waypointOrder = 1
  end
  currentWaypointIndex = currentWaypointIndex + waypointOrder

  thisWaypoint = job.waypoints[currentWaypointIndex]
  x = thisWaypoint.coord[1]
  y = thisWaypoint.coord[2]
  z = thisWaypoint.coord[3]
  
  nav:navigateTo(x, y, z)

  pwr:monitorBattery()

end

bot.setLightColor(0x666666)