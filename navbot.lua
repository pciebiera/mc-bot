--[[

East X+ 5
West X- 4
South Z+ 3
North Z- 2 
Up Y+ 1
Down Y- 0

]]


OffsetX = 959.5
OffsetY = 0.5
OffsetZ = 959.5




function DoNavigate()

  TargetX = 158
  TargetY = 100
  TargetZ = 324

  CurrentX = 0
  CurrentY = 0
  CurrentZ = 0

  DiffX = 0
  DiffY = 0
  DiffZ = 0

  component = require("component")
  Navigating = true
  nav = component.navigation

  sides = require("sides")
  robbo = require("robot")
  
  heading = nav.getFacing()
  NewHeading = false

  TargetX = TargetX - OffsetX
  TargetY = TargetY - OffsetY
  TargetZ = TargetZ - OffsetZ

  DumbSteps = 0
  

  while Navigating do
    CurrentX, CurrrentY, CurrentZ = nav.getPosition()
    -- something wrong with the navigation upgrade or we're outside of the mapped zone
    if CurrentX == nil then
      return 1
    end
    -- Are we done navigating
    if CurrentX == TargetX and CurrentZ == TargetZ then
      return 0
    end
    

    -- Do we need to pick a new heading?
    if DumbSteps == 0 then
      DiffX = math.abs(CurrentX - TargetX)
      --DiffY = math.abs(CurrentY - TargetY)
      DiffZ = math.abs(CurrentZ - TargetZ)
      CurrentHeading = nav.getFacing()

      -- Do we head X?
      if DiffX ~= 0 and DiffX > DiffZ then
        if CurrentX > TargetX then
          heading = sides.negx
        else
          heading = sides.posx
        end
        NewHeading = true
      --[[elseif DiffY ~= 0 and DiffY > DiffZ then
        if CurrentY > TargetY then
          heading = sides.negy
        else
          heading = sides.posy
        end
        NewHeading = true --]]
      elseif DiffZ ~= 0 then
        if CurrentZ > TargetZ then
          heading = sides.negz
        else
          heading = sides.posz
        end
        NewHeading = true
      end

      if heading > 1 then
        while heading ~= nav.getFacing() do robbo.turnRight() end
      end
    end


    -- Can we move in the desired direction
    if robbo.detect() == true then
      if robbo.detectUp() == false then 
        robbo.up()
        SkipMove = true
      else
        --Pick a random direction
        math.randomseed(os.time())
        if math.random(1,2) == 1 then
          turnDirection = robbo.turnLeft
        else
          turnDirection = robbo.turnRight
        end
        sillyTurns = math.random(1,3)
        if sillyTurns == 2 then sillyTurns = 3 end
        for i = sillyTurns, 1, -1
        do
          turnDirection()
        end
        if DumbSteps == 0 then DumbSteps = math.random(3,15) end
      end
    end

    -- Make a move
    if SkipMove ~= true then
      robbo.forward()
      if DumbSteps ~= 0 then 
        DumbSteps = DumbSteps - 1 
      else
        -- stay on the ground
        while robbo.detectDown() == false do
          robbo.down()
        end
      end
    end
    SkipMove = false
  end
end

DoNavigate()