local NV={};

--[[

East X+ 5
West X- 4
South Z+ 3
North Z- 2 
Up Y+ 1
Down Y- 0

]]

local OffsetX = 959.5
local OffsetY = -0.5
local OffsetZ = 959.5

function NV.getPosition()
  local component = require("component")
  local compnav = component.navigation
  local x = 0
  local y = 0
  local z = 0
  x,y,z = compnav.getPosition()
  if x == nil then return nil end
  if y == nil then return nil end
  if z == nil then return nil end
  x = x + OffsetX
  y = y + OffsetY
  z = z + OffsetZ
  return x,y,z
end


function NV:navigateTo(x,y,z)
  local TargetX = x
  local TargetY = y
  local TargetZ = z

  local CurrentX = 0
  local CurrentY = 0
  local CurrentZ = 0

  local DiffX = 0
  local DiffY = 0
  local DiffZ = 0

  local component = require("component")
  local Navigating = true
  local compnav = component.navigation

  local sides = require("sides")
  local robbo = require("robot")
  
  local heading = compnav.getFacing()
  local NewHeading = false
  local currentHeading = heading
  local SkipMove = false

  TargetX = TargetX - OffsetX
  TargetY = TargetY - OffsetY
  TargetZ = TargetZ - OffsetZ

  local DumbSteps = 0
  

  while Navigating do
    -- suspect
    CurrentX, CurrentY, CurrentZ = compnav.getPosition()
    -- something wrong with the navigation upgrade or we're outside of the mapped zone
    if CurrentX == nil then
      return 1
    end
    -- Are we done navigating
    if CurrentX == TargetX and CurrentZ == TargetZ then
      if CurrentY == TargetY then return 0 end
      DumbSteps = 0
      SkipMove = true
      DiffY = math.abs(CurrentY - TargetY)
      io.write("DiffY: " .. DiffY .. " TargetY: " .. TargetY .. " " .. CurrentX .. ", " .. CurrentY .. ", " .. CurrentZ .. "\n")
      if CurrentY > TargetY then
        if robbo.detectDown() == true then return 2 end
        robbo.down()
      else
        if robbo.detectUp() == true then return 2 end
        robbo.up()
      end

    end
    
    -- Do we need to pick a new heading?
    if DumbSteps == 0 then
      DiffX = math.abs(CurrentX - TargetX)
      --DiffY = math.abs(CurrentY - TargetY)
      DiffZ = math.abs(CurrentZ - TargetZ)

      -- Do we head X?
      if DiffX ~= 0 and DiffX > DiffZ then
        if CurrentX > TargetX then
          heading = sides.negx
        else
          heading = sides.posx
        end
        NewHeading = true
      --[[elseif DiffY ~= 0 and DiffY > DiffZ then
        if CurrentY > TargetY then
          heading = sides.negy
        else
          heading = sides.posy
        end
        NewHeading = true --]]
      elseif DiffZ ~= 0 then
        if CurrentZ > TargetZ then
          heading = sides.negz
        else
          heading = sides.posz
        end
        NewHeading = true
      end

      if heading > 1 and NewHeading == true then
        while heading ~= compnav.getFacing() do
          currentHeading = compnav.getFacing()

          --io.write("cmd: " .. heading .. " cur: " .. currentHeading .. "\n")


          if heading == sides.north and currentHeading == sides.east then
            robbo.turnLeft()
          elseif heading == sides.east and currentHeading == sides.south then
            robbo.turnLeft()
          elseif heading == sides.south and currentHeading == sides.west then
            robbo.turnLeft()
          elseif heading == sides.west and currentHeading == sides.north then
            robbo.turnLeft()
          else
            robbo.turnRight()
          end
        end
        NewHeading = false
      end
    end


    -- Can we move in the desired direction
    if robbo.detect() == true and SkipMove ~= true then
      if robbo.detectUp() == false then 
        robbo.up()
        SkipMove = true
      else
        --Pick a random direction
        math.randomseed(os.time())
        if math.random(1,2) == 1 then
          turnDirection = robbo.turnLeft
        else
          turnDirection = robbo.turnRight
        end
        sillyTurns = math.random(1,3)
        if sillyTurns == 2 then sillyTurns = 3 end
        for i = sillyTurns, 1, -1
        do
          turnDirection()
        end
        if DumbSteps == 0 then DumbSteps = math.random(3,15) end
      end
    end

    -- Make a move
    if SkipMove ~= true then
      robbo.forward()
      if DumbSteps ~= 0 then 
        DumbSteps = DumbSteps - 1 
      else
        -- stay on the ground
        while robbo.detectDown() == false do
          robbo.down()
        end
      end
    end
    SkipMove = false
  end
end

return NV;