job = require("missions.mission_01")
working = true
comp = require("component")
nav = require("navigation")
bot = require("robot")
pc = require("computer")
pwr = require("powermgmt")
inv = require("inventorymanagement")

bot.setLightColor(0xffd800)

currentWaypointIndex = 0
waypointOrder = 1
inventorySlots = {}
foundStart = false

cx,cy,cz = nav.getPosition()
for i = 1, #job.waypoints, 1 do
  if job.waypoints[i].coord[1] == cx and job.waypoints[i].coord[2] == cy and job.waypoints[i].coord[3] == cz then
    foundStart = true
    currentWaypointIndex = i
    print("Found waypoint start...")
    break
  end
end

inventorySize = bot.inventorySize()
if inventorySize == nil or inventorySize == 0 then
  working = false
  print("Could not determine inventory size")
else
  print("Found inventory...")
  for i = 1, inventorySize, 1 do
    if bot.count(i) == 0 then
      table.insert(inventorySlots, i)
      print("Found usable inventory slot for job...")
    end
  end
  inv:setAvailableSlots(inventorySlots)
end

if #inventorySlots == 0 then
  working = false
  print("There are no free inventory slots available for this job")
end

if foundStart == false then
  working = false
  print("Could not locate starting point for job")
end

if job == nil then 
  working = false
  print("Could not get job")
end

while working do
  local x = 0
  local y = 0
  local z = 0
  local thisWaypoint = job.waypoints[currentWaypointIndex]
  
  if thisWaypoint.action == "OPTIONAL_CHARGE" then
    if pwr:getEnergyPercentRemaining() < 30 then pwr:chargeAtStation() end
  end
  if thisWaypoint.action == "FILL_INVENTORY" then inv:getAllItems() end
  if thisWaypoint.action == "DUMP_INVENTORY" then inv:putAllItems() end

  if currentWaypointIndex == #job.waypoints then
    waypointOrder = -1
  elseif currentWaypointIndex == 1 then
    waypointOrder = 1
  end
  currentWaypointIndex = currentWaypointIndex + waypointOrder

  thisWaypoint = job.waypoints[currentWaypointIndex]
  x = thisWaypoint.coord[1]
  y = thisWaypoint.coord[2]
  z = thisWaypoint.coord[3]
  print(x)
  nav:navigateTo(x, y, z)

end

comp.robot.setLightColor(0x000000)