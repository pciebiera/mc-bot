local task = {
  ["waypoints"] = {
    {
      ["coord"] = {242,69,328}
    },
    {
      ["coord"] = {257,71,328},
      ["location"] = "Kitchen - Fridge 1",
      ["action"] = "BUILD_LIST",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
        { ["slot"] = 10, ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
        { ["slot"] = 11, ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
        { ["slot"] = 12, ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
        { ["slot"] = 13, ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
        { ["slot"] = 19, ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
        { ["slot"] = 20, ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
        { ["slot"] = 21, ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
        { ["slot"] = 22, ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
        { ["slot"] = 28, ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
        { ["slot"] = 29, ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
        { ["slot"] = 30, ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
        { ["slot"] = 31, ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
        { ["slot"] = 37, ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
        { ["slot"] = 38, ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
        { ["slot"] = 39, ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
        { ["slot"] = 40, ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
        { ["slot"] = 46, ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 },
        { ["slot"] = 47, ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 },
        { ["slot"] = 48, ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 },
        { ["slot"] = 49, ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 }
      }
    },
    {
      ["coord"] = {256,71,324},
      ["location"] = "Kitchen - Fridge 2",
      ["action"] = "BUILD_LIST",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 },
        { ["slot"] = 5,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 },
        { ["slot"] = 6,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 },
        { ["slot"] = 37, ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 },
        { ["slot"] = 38, ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 },
        { ["slot"] = 39, ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 },
        { ["slot"] = 46, ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 },
        { ["slot"] = 47, ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 },
        { ["slot"] = 48, ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 }
      }
    },
    {
      ["coord"] = {252,69,324}
    },
    {
      ["coord"] = {252,70,359},
      ["location"] = "Garden Cloche 1",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,361},
      ["location"] = "Garden Cloche 2",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,363},
      ["location"] = "Garden Cloche 3",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,365},
      ["location"] = "Garden Cloche 4",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,70,367},
      ["location"] = "Garden Cloche 5",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {252,69,368}
    },
    {
      ["coord"] = {256,69,368}
    },
    {
      ["coord"] = {256,70,367},
      ["location"] = "Garden Cloche 6",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,365},
      ["location"] = "Garden Cloche 7",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,363},
      ["location"] = "Garden Cloche 8",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,361},
      ["location"] = "Garden Cloche 9",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,70,359},
      ["location"] = "Garden Cloche 10",
      ["action"] = "SUPPLY_LIST"
    },
    {
      ["coord"] = {256,69,358}
    },
    {
      ["coord"] = {252,69,358}
    },
    {
      ["coord"] = {252,70,359},
      ["location"] = "Garden Cloche 1",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 }
      }
    },
    {
      ["coord"] = {252,70,361},
      ["location"] = "Garden Cloche 2",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 }
      }
    },
    {
      ["coord"] = {252,70,363},
      ["location"] = "Garden Cloche 3",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 }
      }
    },
    {
      ["coord"] = {252,70,365},
      ["location"] = "Garden Cloche 4",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 }
      }
    },
    {
      ["coord"] = {252,70,367},
      ["location"] = "Garden Cloche 5",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 }
      }
    },
    {
      ["coord"] = {252,69,368}
    },
    {
      ["coord"] = {256,69,368}
    },
    {
      ["coord"] = {256,70,367},
      ["location"] = "Garden Cloche 6",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 }
      }
    },
    {
      ["coord"] = {256,70,365},
      ["location"] = "Garden Cloche 7",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 }
      }
    },
    {
      ["coord"] = {256,70,363},
      ["location"] = "Garden Cloche 8",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:onionitem",       ["target"] = 64 }
      }
    },
    {
      ["coord"] = {256,70,361},
      ["location"] = "Garden Cloche 9",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 }
      }
    },
    {
      ["coord"] = {256,70,359},
      ["location"] = "Garden Cloche 10",
      ["action"] = "PICK_CULL",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 }
      }
    },
    {
      ["coord"] = {253,69,339}
    },
    {
      ["coord"] = {253,70,328}
    },
    { 
      ["coord"] = {257,71,328},
      ["location"] = "Kitchen - Fridge 1",
      ["action"] = "STORE_LIST",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:cornitem",        ["target"] = 64 },
        { ["slot"] = 10, ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
        { ["slot"] = 11, ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
        { ["slot"] = 12, ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
        { ["slot"] = 13, ["item"] = "harvestcraft:eggplantitem",    ["target"] = 64 },
        { ["slot"] = 19, ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
        { ["slot"] = 20, ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
        { ["slot"] = 21, ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
        { ["slot"] = 22, ["item"] = "harvestcraft:onionitem",       ["target"] = 64 },
        { ["slot"] = 28, ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
        { ["slot"] = 29, ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
        { ["slot"] = 30, ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
        { ["slot"] = 31, ["item"] = "harvestcraft:bellpepperitem",  ["target"] = 64 },
        { ["slot"] = 37, ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
        { ["slot"] = 38, ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
        { ["slot"] = 39, ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
        { ["slot"] = 40, ["item"] = "harvestcraft:tomatoitem",      ["target"] = 64 },
        { ["slot"] = 46, ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 },
        { ["slot"] = 47, ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 },
        { ["slot"] = 48, ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 },
        { ["slot"] = 49, ["item"] = "harvestcraft:lettuceitem",     ["target"] = 64 }
      }
    },
    {
      ["coord"] = {256,71,324},
      ["location"] = "Kitchen - Fridge 2",
      ["action"] = "STORE_LIST",
      ["managedinventory"] = {
        { ["slot"] = 1,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 },
        { ["slot"] = 2,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 },
        { ["slot"] = 3,  ["item"] = "harvestcraft:garlicitem",      ["target"] = 64 },
        { ["slot"] = 4,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 },
        { ["slot"] = 5,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 },
        { ["slot"] = 6,  ["item"] = "harvestcraft:beanitem",        ["target"] = 64 },
        { ["slot"] = 37, ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 },
        { ["slot"] = 38, ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 },
        { ["slot"] = 39, ["item"] = "harvestcraft:chilipepperitem", ["target"] = 64 },
        { ["slot"] = 46, ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 },
        { ["slot"] = 47, ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 },
        { ["slot"] = 48, ["item"] = "harvestcraft:broccoliitem",    ["target"] = 64 }
      }
    },
    {
      ["coord"] = {248,69,326},
      ["location"] = "Trashcan",
      ["action"] = "DISPOSE_CULL"
    },
    {
      ["coord"] = {248,69,327}
    },
    {
      ["coord"] = {242,69,327}
    },
    {
      ["coord"] = {242,69,325},
      ["location"] = "Robot Charging Station",
      ["action"] = "OPTIONAL_CHARGE",
      ["endpoint"] = true
    }
  },
  ["mission"] = "MANAGE_INVENTORY",
  ["navigation_strategy"] = "ONE_CYCLE"
};
return task;