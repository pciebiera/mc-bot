local INVENTORYMANAGEMENT={};

local comp = require("component")
local bot = require("robot")
local invcnt = comp.inventory_controller
local sides = require("sides")
local slotsAvailable={}
local slotsFull = false


function INVENTORYMANAGEMENT:selectNextOpenSlot()
  local slot = 0
  for slot = 0, #self.slotsAvailable, 1 do
    if bot.count(self.slotsAvailable[slot]) == 0 then
      bot.select(self.slotsAvailable[slot])
      return true
    end
  end
  return false
end

function INVENTORYMANAGEMENT:getOpenSlots()
  local slotCount = bot.inventorySize()
  local openSlots = 0
  local i = 0

  if slotCount == 0 then return 0 end

  for i = 1, slotCount, 1 do
    if bot.count(i) == 0 then openSlots = openSlots + 1 end
  end

  return openSlots;
end

function INVENTORYMANAGEMENT:setAvailableSlots(slots)
  self.slotsAvailable = slots
end

function INVENTORYMANAGEMENT:locateInventoryBlock()
  local attempts = 0
  local a
  local b
  while attempts < 4 do 
    a, b = bot.detect()
    if a == true and b == "solid" then
      if invcnt.getInventorySize(sides.front) ~= nil then return true end
    end
    bot.turnRight() 
    attempts = attempts + 1
  end
  return false
end

function INVENTORYMANAGEMENT:getAllItems()
  local sourceSlotCount = invcnt.getInventorySize(sides.front)
  local sourceSlot = 0
  local sourceInfo = {}
  local destSlot = 0
  local destInfo = {}
  local pickedItems = true
  self.locateInventoryBlock()
  --io.write("Found " .. string.format("%d", sourceSlotCount) .. " slot source inventory.\n Begining transfer.\n\n" )

  while pickedItems do
    pickedItems = false
    for sourceSlot = sourceSlotCount, 1, -1 do
      sourceInfo = invcnt.getStackInSlot(sides.front, sourceSlot)
      if sourceInfo ~= nil then
        --io.write("found " .. string.format("%d ", sourceInfo.size) .. sourceInfo.name .. "\n")
        for destSlot = 1, #self.slotsAvailable, 1 do
          destInfo = invcnt.getStackInInternalSlot(self.slotsAvailable[destSlot])
          if destInfo ~= nil then
            if destInfo.name == sourceInfo.name and destInfo.size < destInfo.maxSize then
              local transferQty = destInfo.maxSize - destInfo.size
              if transferQty > sourceInfo.size then transferQty = sourceInfo.size end
              bot.select(self.slotsAvailable[destSlot])
              --io.write("  transferring " .. string.format("%d", transferQty) .. " " .. sourceInfo.name .. " from S:" .. string.format("%d", sourceSlot) .. " to D:" .. string.format("%d", self.slotsAvailable[destSlot]) .. "\n")
              invcnt.suckFromSlot(sides.front, sourceSlot, transferQty)
              pickedItems = true
              break
            end
          else
          end
        end

        if pickedItems == false then
          for destSlot = 1, #self.slotsAvailable, 1 do
            destInfo = invcnt.getStackInInternalSlot(self.slotsAvailable[destSlot])
            if destInfo == nil then
              bot.select(self.slotsAvailable[destSlot])
              --io.write("  transferring " .. string.format("%d", sourceInfo.size) .. " " .. sourceInfo.name .. " from S:" .. string.format("%d", sourceSlot) .. " to D:" .. string.format("%d", self.slotsAvailable[destSlot]) .. "\n")
              invcnt.suckFromSlot(sides.front, sourceSlot, sourceInfo.size)
              pickedItems = true
              break
            end
          end
        end

        --if pickedItems == false then io.write("  no room for this item now.\n") end
      end
      if pickedItems then break end
    end
  end
  --io.write("\ndone.\n\n")
end

function INVENTORYMANAGEMENT:getShoppingList(managedInventory)
  self.locateInventoryBlock()
  local shoppingList = {}
  local i = 0
  local e = 0
  local itemFound = false
  local thisItem = nil
  local thisQty = 0

  for i = 1, #managedInventory, 1 do
    --determine if this item needs attention
    local targetData = invcnt.getStackInSlot(sides.front, managedInventory[i].slot)
    
    if targetData == nil then
      thisQty = managedInventory[i].target
    elseif targetData.name == managedInventory[i].item and targetData.size < managedInventory[i].target then
      thisQty = managedInventory[i].target - targetData.size
    end

    thisItem = { ["item"] = managedInventory[i].item, ["qty"] = thisQty }

    if thisQty > 0 then
      for e = 1, #shoppingList, 1 do
        if shoppingList[e].item == thisItem.item then
          shoppingList[e].qty = shoppingList[e].qty + thisItem.qty
          itemFound = true
          break
        end
      end
      if itemFound ~= true then table.insert(shoppingList, thisItem) end
      io.write("added " .. thisItem.qty .. " " .. thisItem.item .. " to the shopping list.\n")
    end

    thisQty = 0
    thisItem = nil
    itemFound = false
  end

  return shoppingList
end

function INVENTORYMANAGEMENT:goShopping(shoppingList)
  if self.slotsFull == true then return 1 end
  self.locateInventoryBlock()
  local i = 0
  local e = 0
  local s = 0
  local sourceSlots = invcnt.getInventorySize(sides.front)
  local sourceInfo = {}
  local transferRequestQty = 0
  local transferQty = 0
  local destInfo = {}
  local destSlot = 0

  for sourceSlot = 1, sourceSlots, 1 do
    io.write("checking slot: " .. sourceSlot .. "\n")
    sourceInfo = invcnt.getStackInSlot(sides.front, sourceSlot)
    if sourceInfo ~= nil then
      --check if we need this item
      io.write("Slot contains: " .. sourceInfo.name .. "\n")
      for e = 1, #shoppingList, 1 do
        io.write("\tlist: #" .. e .. " " .. shoppingList[e].item .. "\n")
        if sourceInfo.name == shoppingList[e].item then
          if sourceInfo.size > shoppingList[e].qty then
            transferRequestQty = shoppingList[e].qty
          else
            transferRequestQty = sourceInfo.size
          end

          if shoppingList[e].qty == 0 then break end

          for s = 1, #self.slotsAvailable, 1 do

            destInfo = invcnt.getStackInInternalSlot(self.slotsAvailable[s])
            if destInfo == nil or (destInfo.name == sourceInfo.name and destInfo.size < destInfo.maxSize) then
            
              if destInfo ~= nil and transferRequestQty >= destInfo.maxSize - destInfo.size then
                transferQty = destInfo.maxSize - destInfo.size
              else
                if transferRequestQty > sourceInfo.maxSize then
                  transferQty = sourceInfo.maxSize
                else
                  transferQty = transferRequestQty
                end
              end

              bot.select(self.slotsAvailable[s])
              io.write("taking " .. transferQty .. " " .. sourceInfo.name .. "\n")
              invcnt.suckFromSlot(sides.front, sourceSlot, transferQty)

              -- take these off the shopping list
              shoppingList[e].qty = shoppingList[e].qty - transferQty
              transferRequestQty = transferRequestQty - transferQty
              if transferRequestQty == 0 then break end
            end
          end
        end
      end
    end
  end
end

function INVENTORYMANAGEMENT:restockInventory(managedInventory)
  self.locateInventoryBlock()
  local sourceSlot = 0
  local sourceInfo = {}
  local destIndex = 0
  local destInfo = {}
  local transferRequestQty = 0
  local transferQty = 0
  
  for sourceSlot = 1, #self.slotsAvailable, 1 do
    sourceInfo = invcnt.getStackInInternalSlot(self.slotsAvailable[sourceSlot])
    if sourceInfo ~= nil then
      io.write("found " .. sourceInfo.size .. " " .. sourceInfo.name .. " in slot " .. self.slotsAvailable[sourceSlot] .. ".\n")
      for destIndex = 1, #managedInventory, 1 do
        io.write(" comparing to managed inventory " .. managedInventory[destIndex].item .. "\n")
        if sourceInfo.name == managedInventory[destIndex].item then
          io.write("  found... ")
          destInfo = invcnt.getStackInSlot(sides.front, managedInventory[destIndex].slot)
          if destInfo == nil then
            transferRequestQty = managedInventory[destIndex].target
          else
            transferRequestQty = managedInventory[destIndex].target - destInfo.size
          end
          
          if transferRequestQty ~= 0 then
            if transferRequestQty > sourceInfo.size then transferRequestQty = sourceInfo.size end
            io.write(" Will transfer " .. transferQty .. " " .. sourceInfo.name .. " to slot " .. managedInventory[destIndex].slot .. "\n" )
            bot.select(self.slotsAvailable[sourceSlot])
            invcnt.dropIntoSlot(sides.front, managedInventory[destIndex].slot, transferRequestQty)
            sourceInfo.size = sourceInfo.size - transferRequestQty
            if sourceInfo.size == 0 then break end
          end
        end
      end
    end
  end
end

function INVENTORYMANAGEMENT:pickItemForCulling(managedInventory)
  self.locateInventoryBlock()
  sourceSlot = 0
  sourceInfo = {}
  sourceIndex = 0
  transferRequestQty = 0
  transferQty = 0
  destIndex = 0
  destInfo = {}

  for sourceIndex = 1, #managedInventory, 1 do
    sourceInfo = invcnt.getStackInSlot(sides.front, managedInventory[sourceIndex].slot)
    if sourceInfo ~= nil then
      if sourceInfo.name ~= managedInventory[sourceIndex].item then 
        transferRequestQty = sourceInfo.size
      else
        if sourceInfo.size > managedInventory[sourceIndex].target then transferRequestQty = sourceInfo.size - managedInventory[sourceIndex].target end
      end
      if transferRequestQty ~= 0 then
        for destIndex = 1, #self.slotsAvailable, 1 do
          destInfo = invcnt.getStackInInternalSlot(self.slotsAvailable[destIndex])
          if destInfo ~= nil then
            if destInfo.name == managedInventory[sourceIndex].item then
              transferQty = destInfo.maxSize - destInfo.size
              if transferQty > transferRequestQty then transferQty = transferRequestQty end
            end
          else
            if transferRequestQty > sourceInfo.maxSize then
              transferQty = sourceInfo.maxSize
            else
              transferQty = transferRequestQty
            end
          end
          if transferQty > 0 then
            bot.select(self.slotsAvailable[destIndex])
            invcnt.suckFromSlot(sides.front, managedInventory[sourceIndex].slot, transferQty)
            transferRequestQty = transferRequestQty - transferQty
          end
          if transferRequestQty == 0 then break end
        end
      end
    end
  end
end

function INVENTORYMANAGEMENT:putAllItems()
  self.locateInventoryBlock()
  local putItems = true
  local sourceSlot = 0
  local destSlot = 0
  local destSlotCount = invcnt.getInventorySize(sides.front)

  local sourceInfo={}
  local destInfo={}
  local transferQty = 0
  local pushedItems = true
  
  while pushedItems == true do
    pushedItems = false
    for sourceSlot = 1, #self.slotsAvailable, 1 do
      bot.select(self.slotsAvailable[sourceSlot])
      if bot.count(self.slotsAvailable[sourceSlot]) > 0 then
        sourceInfo = invcnt.getStackInInternalSlot(self.slotsAvailable[sourceSlot])
        for destSlot = 1, destSlotCount, 1 do
          destInfo = invcnt.getStackInSlot(sides.front, destSlot)
          if destInfo == nil or (destInfo.name == sourceInfo.name and destInfo.size < destInfo.maxSize)  then
            if destInfo == nil then
              transferQty = sourceInfo.size
            else
              transferQty = destInfo.maxSize - destInfo.size
              if transferQty > sourceInfo.size then transferQty = sourceInfo.size end
            end
            bot.select(self.slotsAvailable[sourceSlot])
            invcnt.dropIntoSlot(sides.front, destSlot, transferQty)
            pushedItems = true
            break
          end
          if pushedItems == true then break end
        end
      end
      if pushedItems == true then break end
    end
  end
end
return INVENTORYMANAGEMENT;